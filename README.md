## Code Challenge
Code challenge submission for a senior software engineering role.

It is a simple application that reads a csv file, processes the data and 
creates an output csv based on a mixture of both the input and persistent data.

### Deps
 * [java SE 15](https://www.oracle.com/java/technologies/javase/jdk15-archive-downloads.html)
 * [make](https://www.gnu.org/software/make/manual/make.html)
 * [mvn](https://maven.apache.org/what-is-maven.html)

### Usage
The input and output file paths are parameterized in `src/main/resources/application.yml` 
via the property paths `csv.inputPath` and `csv.outputPath` respectively.

Sample files are supplied in `src/main/resources/csv`.


 * to install maven dependencies:
    ```shell
      $ make install
    ```
 * to run tests:
    ```shell
      $ make test
    ```
 * to run locally:
    ```shell
      $ make run
    ```

### Notes
 * Static lists in repo classes act a faux datastore to hold persistent data (mainly useful for route fares). 
   This may look a bit weird but is meant to be a lightweight way to mimic some real storage layer.
   It felt like over-engineering to introduce an entire db service + supporting migrations just to hold the data present here. 
 * An annotated version of `input.csv` is available at `src/test/resources/csv/input-annotated.yml`. 
   This file is used for a full integration test in `src/test/java/org/example/AppIntegrationTest.java`.
   Each trip tests a key corner case in the business logic. 
   Each trip is annotated to explain the corner case it tests for full coverage.
   These cases are tested individually in `src/test/java/org/example/app/service/RecordServiceTest.java`.
