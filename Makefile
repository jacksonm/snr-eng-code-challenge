test:
	mvn clean package install

install:
	mvn clean -Dmaven.test.skip=true install

run:
	mvn spring-boot:run
