package org.example.app.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.example.app.entity.Trip;
import org.example.app.service.dto.TripDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class CsvService {

	private static final List<String> headers =
			Arrays.asList(
					"Started",
					"Finished",
					"DurationSecs",
					"FromStopId",
					"ToStopId",
					"ChargeAmount",
					"CompanyId",
					"BusID",
					"PAN",
					"Status");
	private static final ObjectMapper mapper = new ObjectMapper();

	private final DateTimeFormatter formatter;

	@Autowired
	public CsvService(DateTimeFormatter formatter) {
		this.formatter = formatter;
	}

	public List<TripDto> parseTravelData(String path) throws IOException {
		CSVParser parser =
				CSVParser.parse(
						new File(path), Charset.defaultCharset(), CSVFormat.DEFAULT.withFirstRecordAsHeader());

		List<TripDto> tripRecords = new ArrayList<>();
		for (CSVRecord r : parser.getRecords()) {
			TripDto t = mapper.convertValue(r.toMap(), TripDto.class);
			tripRecords.add(t);
		}
		return tripRecords;
	}

	public void createTripsDataCsv(List<Trip> trips, String path) {
		try {
			BufferedWriter writer = Files.newBufferedWriter(Paths.get(path));
			CSVPrinter printer =
					new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(headers.toArray(new String[0])));
			for (Trip t : trips) {
				printer.printRecord(
						t.getStarted() == null ? null : t.getStarted().format(formatter),
						t.getFinished() == null ? null : t.getFinished().format(formatter),
						t.getDuration(),
						t.getFromId(),
						t.getToId(),
						t.getPrice() == null ? null : String.format("%.2f", t.getPrice()),
						t.getCompanyId(),
						t.getBusId(),
						t.getPan(),
						t.getStatus());
			}
			printer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
