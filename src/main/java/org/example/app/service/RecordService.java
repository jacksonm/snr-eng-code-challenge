package org.example.app.service;

import org.example.app.entity.*;
import org.example.app.repo.BusRepo;
import org.example.app.repo.RouteRepo;
import org.example.app.repo.StopRepo;
import org.example.app.service.dto.TripDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;

@Component
public class RecordService {

	private final DateTimeFormatter formatter;
	private final BusRepo busRepo;
	private final RouteRepo routeRepo;
	private final StopRepo stopRepo;

	@Autowired
	public RecordService(DateTimeFormatter formatter, BusRepo busRepo, RouteRepo routeRepo, StopRepo stopRepo) {
		this.formatter = formatter;
		this.busRepo = busRepo;
		this.routeRepo = routeRepo;
		this.stopRepo = stopRepo;
	}

	public List<Trip> processTravelRecords(List<TripDto> records) throws EntityNotFoundException {
		List<Trip> trips = new ArrayList<>(emptyList());

		TripDto startTrip = null;
		for (TripDto t : records) {
			switch (t.tapType) {
				case "ON" -> {
					if (startTrip != null && startTrip.tapType.equals("ON")) {
						trips.add(finalizeTrip(startTrip, null));
					}

					startTrip = t;
				}
				case "OFF" -> {
					if (startTrip != null && !startTrip.busId.equals(t.busId)) {
						trips.add(finalizeTrip(startTrip, null));
						trips.add(finalizeTrip(null, t));
					} else {
						trips.add(finalizeTrip(startTrip, t));
					}

					startTrip = null;
				}
				default -> System.out.println("PANIC: RecordService - processTravelRecords - unreachable");
			}
		}

		if (startTrip != null) {
			trips.add(finalizeTrip(startTrip, null));
		}

		return trips;
	}

	public Trip finalizeTrip(TripDto prev, TripDto t) throws EntityNotFoundException {
		Trip trip;

		if (prev == null) {
			trip = finalizeIncompleteOffTrip(t);
		} else if (t == null) {
			trip = finalizeIncompleteOnTrip(prev);
		} else {
			trip = finalizeCompleteTrip(prev, t);
		}

		return trip;
	}

	private Trip finalizeCompleteTrip(TripDto prev, TripDto t) throws EntityNotFoundException {
		String pan = t.pan == null ? prev.pan : t.pan;
		String busId = t.busId == null ? prev.busId : t.busId;
		Bus bus = busRepo.getById(busId).orElseThrow(() -> new EntityNotFoundException(Bus.class, busId));
		Stop from = stopRepo.getStopById(prev.stopId).orElseThrow(() -> new EntityNotFoundException(Stop.class, prev.stopId));
		Stop to = stopRepo.getStopById(t.stopId).orElseThrow(() -> new EntityNotFoundException(Stop.class, t.stopId));

		Route route = routeRepo.getRouteByStopIds(Arrays.asList(prev.stopId, t.stopId))
				.orElseThrow(() -> new EntityNotFoundException(Route.class, prev.stopId + " " + t.stopId));

		Double price;
		if (prev.stopId.equals(t.stopId)) {
			price = null;
		} else {
			price = route.getPrice();
		}

		return new Trip(
				LocalDateTime.parse(prev.dateTime, formatter),
				LocalDateTime.parse(t.dateTime, formatter),
				from, to, price, bus, pan, TripStatus.COMPLETE
		);
	}

	private Trip finalizeIncompleteOnTrip(TripDto t) throws EntityNotFoundException {
		Bus bus = busRepo.getById(t.busId).orElseThrow(() -> new EntityNotFoundException(Bus.class, t.busId));
		Stop from = stopRepo.getStopById(t.stopId).orElseThrow(() -> new EntityNotFoundException(Stop.class, t.stopId));

		Route route = routeRepo.getHighestPriceByStopId(t.stopId)
				.orElseThrow(() -> new EntityNotFoundException(Route.class, t.stopId));

		return new Trip(LocalDateTime.parse(t.dateTime, formatter), null,
				from, null, route.getPrice(), bus, t.pan, TripStatus.INCOMPLETE
		);
	}

	private Trip finalizeIncompleteOffTrip(TripDto t) throws EntityNotFoundException {
		Bus bus = busRepo.getById(t.busId).orElseThrow(() -> new EntityNotFoundException(Bus.class, t.busId));
		Stop to = stopRepo.getStopById(t.stopId).orElseThrow(() -> new EntityNotFoundException(Stop.class, t.stopId));

		Route route = routeRepo.getHighestPriceByStopId(t.stopId)
				.orElseThrow(() -> new EntityNotFoundException(Route.class, t.stopId));

		return new Trip(null, LocalDateTime.parse(t.dateTime, formatter),
				null, to, route.getPrice(), bus, t.pan, TripStatus.INCOMPLETE
		);
	}
}
