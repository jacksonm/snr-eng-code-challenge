package org.example.app.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TripDto {

	@JsonProperty("ID")
	public String id;

	@JsonProperty("DateTimeUTC")
	public String dateTime;

	@JsonProperty("TapType")
	public String tapType;

	@JsonProperty("StopId")
	public String stopId;

	@JsonProperty("CompanyId")
	public String companyId;

	@JsonProperty("BusID")
	public String busId;

	@JsonProperty("PAN")
	public String pan;

	// Default constructor needed for object mapper
	public TripDto() {}

	public TripDto(
			String id,
			String dateTime,
			String tapType,
			String stopId,
			String companyId,
			String busId,
			String pan) {
		this.id = id;
		this.dateTime = dateTime;
		this.tapType = tapType;
		this.stopId = stopId;
		this.companyId = companyId;
		this.busId = busId;
		this.pan = pan;
	}
}
