package org.example.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.format.DateTimeFormatter;

@Configuration
@EnableConfigurationProperties
public class ApplicationConfig {

	@Value("${csv.inputPath}")
	private String inputPath;

	@Value("${csv.outputPath}")
	private String outputPath;

	@Bean
	public String inputPath() {
		return inputPath;
	}

	@Bean
	public String outputPath() {
		return outputPath;
	}

	@Bean
	public DateTimeFormatter formatter() {
		return DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	}
}
