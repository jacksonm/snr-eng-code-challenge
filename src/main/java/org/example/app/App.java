package org.example.app;

import org.example.app.entity.EntityNotFoundException;
import org.example.app.entity.Trip;
import org.example.app.service.CsvService;
import org.example.app.service.RecordService;
import org.example.app.service.dto.TripDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class App implements CommandLineRunner {

	private final CsvService csvService;
	private final RecordService recordsService;
	private final String inputPath;
	private final String outputPath;

	@Autowired
	public App(CsvService csvService, RecordService recordsService, String inputPath, String outputPath) {
		this.csvService = csvService;
		this.recordsService = recordsService;
		this.inputPath = inputPath;
		this.outputPath = outputPath;
	}

	@Override
	public void run(String... args) throws EntityNotFoundException, IOException {
		main(args);
	}

	public void main(String[] args) throws IOException, EntityNotFoundException {
		GlobalExceptionHandler globalExceptionHandler = new GlobalExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(globalExceptionHandler);

		List<TripDto> records = csvService.parseTravelData(inputPath);
		List<Trip> trips = recordsService.processTravelRecords(records);

		csvService.createTripsDataCsv(trips, outputPath);
	}
}
