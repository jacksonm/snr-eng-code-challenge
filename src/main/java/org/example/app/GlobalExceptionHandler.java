package org.example.app;

import org.example.app.entity.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlobalExceptionHandler implements Thread.UncaughtExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@Override
	public void uncaughtException(Thread t, Throwable e) {
		LOGGER.error("Caught Exception: " + e.getMessage());
	}

	public void uncaughtEntityNotFoundException(Thread t, EntityNotFoundException e) {
		LOGGER.error("Caught EntityNotFoundException: " + e.getMessage());
	}
}
