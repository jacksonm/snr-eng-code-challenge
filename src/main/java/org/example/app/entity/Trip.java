package org.example.app.entity;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class Trip {

	private static final ZoneOffset zone = ZoneOffset.UTC;

	private final LocalDateTime started;
	private final LocalDateTime finished;
	private final Stop from;
	private final Stop to;
	private final Double price;
	private final Bus bus;
	private final String pan;
	private final TripStatus status;

	public Trip(
			LocalDateTime started,
			LocalDateTime finished,
			Stop from,
			Stop to,
			Double price,
			Bus bus,
			String pan,
			TripStatus status) {
		this.started = started;
		this.finished = finished;
		this.from = from;
		this.to = to;
		this.price = price;
		this.bus = bus;
		this.pan = pan;
		this.status = status;
	}

	@Override
	public String toString() {
		return "{ \n"
				+ "\tstarted: "
				+ this.started
				+ "\n"
				+ "\tfinished: "
				+ this.finished
				+ "\n"
				+ "\tfrom: "
				+ this.getFromId()
				+ "\n"
				+ "\tto: "
				+ this.getToId()
				+ "\n"
				+ "\tprice: "
				+ this.getPrice()
				+ "\n"
				+ "\tbus: "
				+ this.getBusId()
				+ "\n"
				+ "\tpan: "
				+ this.pan
				+ "\n"
				+ "\tstatus: "
				+ this.status
				+ "\n"
				+ "}";
	}

	public LocalDateTime getStarted() {
		return this.started;
	}

	public LocalDateTime getFinished() {
		return finished;
	}

	public Long getDuration() {
		if (this.finished == null || this.started == null) {
			return null;
		}

		return this.finished.toEpochSecond(zone) - this.started.toEpochSecond(zone);
	}

	public Stop getFrom() {
		return from;
	}

	public String getFromId() {
		return this.from == null ? null : this.getFrom().getId();
	}

	public Stop getTo() {
		return to;
	}

	public String getToId() {
		return this.to == null ? null : this.getTo().getId();
	}

	public Double getPrice() {
		return this.price;
	}

	public Company getCompany() {
		return this.bus.getCompany();
	}

	public String getCompanyId() {
		return this.bus == null ? null : this.getCompany().getId();
	}

	public Bus getBus() {
		return bus;
	}

	public String getBusId() {
		return this.bus == null ? null : this.getBus().getId();
	}

	public String getPan() {
		return pan;
	}

	public TripStatus getStatus() {
		return status;
	}
}
