package org.example.app.entity;

public class Company {

	private final String id;

	public Company(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
}
