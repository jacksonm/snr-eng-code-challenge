package org.example.app.entity;

import java.util.List;

public class Route {

	private final List<Stop> stops;
	private final double price;

	public Route(List<Stop> stops, double price) {
		this.stops = stops;
		this.price = price;
	}

	public List<Stop> getStops() {
		return stops;
	}

	public double getPrice() {
		return price;
	}
}
