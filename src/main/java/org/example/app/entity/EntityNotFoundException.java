package org.example.app.entity;

public class EntityNotFoundException extends Exception {
	public EntityNotFoundException(Class<?> clazz, String id) {
		super("Unable to find Entity: " + clazz.getCanonicalName() + " with id: " + id);
	}
}
