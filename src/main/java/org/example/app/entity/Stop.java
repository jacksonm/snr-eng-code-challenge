package org.example.app.entity;

public class Stop {

	private final String id;

	public Stop(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
}
