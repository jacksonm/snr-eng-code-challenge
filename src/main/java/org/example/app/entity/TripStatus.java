package org.example.app.entity;

public enum TripStatus {
	COMPLETE,
	INCOMPLETE
}
