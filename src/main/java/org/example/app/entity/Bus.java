package org.example.app.entity;

public class Bus {

	private final String id;
	private final Company company;

	public Bus(Company company, String id) {
		this.id = id;
		this.company = company;
	}

	public String getId() {
		return id;
	}

	public Company getCompany() {
		return company;
	}
}
