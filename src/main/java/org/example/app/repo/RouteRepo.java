package org.example.app.repo;

import org.example.app.entity.Route;
import org.example.app.entity.Stop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class RouteRepo {

	private final List<Route> routes;

	@Autowired
	public RouteRepo(StopRepo stopRepo) {
		Stop s1 = stopRepo.getStopById("Stop1").get();
		Stop s2 = stopRepo.getStopById("Stop2").get();
		Stop s3 = stopRepo.getStopById("Stop3").get();

		this.routes =
				Arrays.asList(
						new Route(Arrays.asList(s1, s2), 3.25d),
						new Route(Arrays.asList(s2, s3), 5.50d),
						new Route(Arrays.asList(s3, s1), 7.30d));
	}

	public Optional<Route> getRouteByStopIds(List<String> ids) {
		return routes.stream()
				.filter(
						route ->
								route.getStops().stream()
										.map(Stop::getId)
										.collect(Collectors.toList())
										.containsAll(ids))
				.findFirst();
	}

	public Optional<Route> getHighestPriceByStopId(String stopId) {
		return routes.stream()
				.filter(route -> route.getStops().stream().anyMatch(s -> s.getId().equals(stopId)))
				.collect(Collectors.toList())
				.stream()
				.max(Comparator.comparing(Route::getPrice))
				.stream()
				.findFirst();
	}
}
