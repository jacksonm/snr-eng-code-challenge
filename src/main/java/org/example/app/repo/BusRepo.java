package org.example.app.repo;

import org.example.app.entity.Bus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class BusRepo {

	private final List<Bus> buses;

	@Autowired
	public BusRepo(CompanyRepo companyRepo) {
		this.buses =
				Arrays.asList(
						new Bus(companyRepo.getById("Company1").get(), "Bus37"),
						new Bus(companyRepo.getById("Company1").get(), "Bus38"));
	}

	public Optional<Bus> getById(String id) {
		return buses.stream().filter(b -> b.getId().equals(id)).findFirst();
	}
}
