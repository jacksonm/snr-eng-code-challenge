package org.example.app.repo;

import org.example.app.entity.Stop;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class StopRepo {

	private final List<Stop> stops;

	public StopRepo() {
		this.stops = Arrays.asList(new Stop("Stop1"), new Stop("Stop2"), new Stop("Stop3"));
	}

	public Optional<Stop> getStopById(String id) {
		return stops.stream().filter(s -> s.getId().equals(id)).findFirst();
	}
}
