package org.example.app.repo;

import org.example.app.entity.Company;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class CompanyRepo {

	private final List<Company> companies;

	public CompanyRepo() {
		this.companies = Arrays.asList(new Company("Company1"), new Company("Company2"));
	}

	public Optional<Company> getById(String id) {
		return companies.stream().filter(c -> c.getId().equals(id)).findFirst();
	}
}
