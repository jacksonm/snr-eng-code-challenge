package org.example;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.example.app.App;
import org.example.app.entity.EntityNotFoundException;
import org.example.app.service.CsvService;
import org.example.app.service.RecordService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AppIntegrationTest {

	@Autowired private CsvService csvService;
	@Autowired private RecordService recordService;
	@Autowired private String inputPath;
	@Autowired private String outputPath;

	private App app;

	@BeforeAll
	public void setUp() {
		app = new App(csvService, recordService, inputPath, outputPath);
	}

	@Test
	void shouldParseTravelRecords() throws IOException, EntityNotFoundException {
		app.run();

		CSVParser parser =
				CSVParser.parse(
						new File(outputPath), Charset.defaultCharset(), CSVFormat.DEFAULT.withFirstRecordAsHeader());

		List<Map<String, String>> actual = new ArrayList<>();
		for (CSVRecord r : parser.getRecords()) {
			actual.add(r.toMap());
		}

		// Leading OFF trip
		assertTripRecord(actual.get(0), "","22-01-2018 01:00:00","","","Stop1","7.30","Company1","Bus37","1","INCOMPLETE");

		// Regular trip
		assertTripRecord(actual.get(1), "22-01-2018 02:00:00","22-01-2018 02:05:00","300","Stop1","Stop2","3.25","Company1","Bus37","2","COMPLETE");

		// Conn ON trip
		assertTripRecord(actual.get(2), "22-01-2018 03:00:00","","","Stop1","","7.30","Company1","Bus37","3","INCOMPLETE");
		assertTripRecord(actual.get(3), "22-01-2018 03:05:00","22-01-2018 03:10:00","300","Stop1","Stop1","","Company1","Bus37","3","COMPLETE");

		// Conn OFF trip
		assertTripRecord(actual.get(4), "","22-01-2018 04:05:00","","","Stop1","7.30","Company1","Bus37","4","INCOMPLETE");

		// Regular trip, different buses
		assertTripRecord(actual.get(5), "22-01-2018 05:00:00","","","Stop1","","7.30","Company1","Bus37","5","INCOMPLETE");
		assertTripRecord(actual.get(6), "","22-01-2018 05:05:00","","","Stop1","7.30","Company1","Bus38","5","INCOMPLETE");

		// Cancelled trip
		assertTripRecord(actual.get(7), "22-01-2018 06:00:00","22-01-2018 06:05:00","300","Stop1","Stop1","","Company1","Bus37","6","COMPLETE");

		// Trailing ON trip
		assertTripRecord(actual.get(8), "22-01-2018 07:00:00","","","Stop1","","7.30","Company1","Bus37","7","INCOMPLETE");
	}

	private void assertTripRecord(Map<String, String> r, String s, String f, String d, String fid, String tid, String ca, String ci, String bid, String pan, String status) {
		assertEquals(r.get("Started"), s);
		assertEquals(r.get("Finished"), f);
		assertEquals(r.get("DurationSecs"), d);
		assertEquals(r.get("FromStopId"), fid);
		assertEquals(r.get("ToStopId"), tid);
		assertEquals(r.get("ChargeAmount"), ca);
		assertEquals(r.get("CompanyId"), ci);
		assertEquals(r.get("BusID"), bid);
		assertEquals(r.get("PAN"), pan);
		assertEquals(r.get("Status"), status);

	}
}
