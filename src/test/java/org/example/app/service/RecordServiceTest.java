package org.example.app.service;

import org.example.app.entity.EntityNotFoundException;
import org.example.app.entity.Trip;
import org.example.app.entity.TripStatus;
import org.example.app.repo.BusRepo;
import org.example.app.repo.RouteRepo;
import org.example.app.repo.StopRepo;
import org.example.app.service.dto.TripDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RecordServiceTest {

	private static Double maxPriceForStop1;
	private static Double maxPriceForStop2;
	private static Double maxPriceForStop3;

	@Autowired private DateTimeFormatter formatter;

	@Autowired private BusRepo busRepo;

	@Autowired private RouteRepo routeRepo;

	@Autowired private StopRepo stopRepo;

	private RecordService service;

	@BeforeAll
	public void setUp() {
		service = new RecordService(formatter, busRepo, routeRepo, stopRepo);
		maxPriceForStop1 = routeRepo.getHighestPriceByStopId("Stop1").get().getPrice();
		maxPriceForStop2 = routeRepo.getHighestPriceByStopId("Stop2").get().getPrice();
		maxPriceForStop3 = routeRepo.getHighestPriceByStopId("Stop3").get().getPrice();
	}

	@Test
	void shouldFinalizeTrip() throws EntityNotFoundException {
		List<TripDto> data =
				Arrays.asList(
						new TripDto(
								"1", "22-01-2018 13:00:00", "ON", "Stop1", "Company1", "Bus37", "5500005555555559"),
						new TripDto(
								"2",
								"22-01-2018 13:05:00",
								"OFF",
								"Stop2",
								"Company1",
								"Bus37",
								"5500005555555559"));

		List<Trip> actual = service.processTravelRecords(data);
		assertEquals(actual.size(), 1);
		assertEquals(actual.get(0).getStarted().format(formatter), "22-01-2018 13:00:00");
		assertEquals(actual.get(0).getFinished().format(formatter), "22-01-2018 13:05:00");
		assertEquals(actual.get(0).getFromId(), "Stop1");
		assertEquals(actual.get(0).getCompanyId(), "Company1");
		assertEquals(actual.get(0).getBusId(), "Bus37");
		assertEquals(actual.get(0).getPrice(), 3.25d);
		assertEquals(actual.get(0).getPan(), "5500005555555559");
		assertEquals(actual.get(0).getStatus(), TripStatus.COMPLETE);
	}

	@Test
	void shouldFinalizeSingleTripWithNoTapOFF() throws EntityNotFoundException {
		List<TripDto> data =
				Collections.singletonList(
						new TripDto(
								"1",
								"22-01-2018 13:00:00",
								"ON",
								"Stop1",
								"Company1",
								"Bus37",
								"5500005555555559"));

		List<Trip> actual = service.processTravelRecords(data);
		assertEquals(actual.size(), 1);
		assertEquals(actual.get(0).getPrice(), maxPriceForStop1);
		assertEquals(actual.get(0).getStatus(), TripStatus.INCOMPLETE);
	}

	@Test
	void shouldFinalizeSingleTripWithNoTapON() throws EntityNotFoundException {
		List<TripDto> data =
				Collections.singletonList(
						new TripDto(
								"1",
								"22-01-2018 13:00:00",
								"OFF",
								"Stop1",
								"Company1",
								"Bus37",
								"5500005555555559"));

		List<Trip> actual = service.processTravelRecords(data);
		assertEquals(actual.size(), 1);
		assertEquals(actual.get(0).getPrice(), maxPriceForStop1);
		assertEquals(actual.get(0).getStatus(), TripStatus.INCOMPLETE);
	}

	@Test
	void shouldFinalizeConsecutiveTripsWithNoTapON() throws EntityNotFoundException {
		List<TripDto> data =
				Arrays.asList(
						new TripDto(
								"1",
								"22-01-2018 13:00:00",
								"OFF",
								"Stop2",
								"Company1",
								"Bus37",
								"5500005555555559"),
						new TripDto(
								"2",
								"22-01-2018 13:05:00",
								"OFF",
								"Stop3",
								"Company1",
								"Bus37",
								"5500005555555559"));

		List<Trip> actual = service.processTravelRecords(data);
		assertEquals(actual.size(), 2);
		assertEquals(actual.get(0).getPrice(), maxPriceForStop2);
		assertEquals(actual.get(1).getPrice(), maxPriceForStop3);
	}

	@Test
	void shouldFinalizeConsecutiveTripsWithNoTapOFF() throws EntityNotFoundException {
		List<TripDto> data =
				Arrays.asList(
						new TripDto(
								"1", "22-01-2018 13:00:00", "ON", "Stop1", "Company1", "Bus37", "5500005555555559"),
						new TripDto(
								"2",
								"22-01-2018 13:05:00",
								"ON",
								"Stop2",
								"Company1",
								"Bus37",
								"5500005555555559"));

		List<Trip> actual = service.processTravelRecords(data);
		assertEquals(actual.size(), 2);
		assertEquals(actual.get(0).getPrice(), maxPriceForStop1);
		assertEquals(actual.get(1).getPrice(), maxPriceForStop2);
	}

	@Test
	void shouldFinalizeTripsWithDifferentBusIds() throws EntityNotFoundException {
		List<TripDto> data =
				Arrays.asList(
						new TripDto(
								"1", "22-01-2018 13:00:00", "ON", "Stop1", "Company1", "Bus37", "5500005555555559"),
						new TripDto(
								"2",
								"22-01-2018 13:05:00",
								"OFF",
								"Stop2",
								"Company1",
								"Bus38",
								"5500005555555559"));

		List<Trip> actual = service.processTravelRecords(data);
		assertEquals(actual.size(), 2);
		assertEquals(actual.get(0).getPrice(), maxPriceForStop1);
		assertEquals(actual.get(1).getPrice(), maxPriceForStop2);
	}

	@Test
	void shouldFinalizeCancelledTrip() throws EntityNotFoundException {
		List<TripDto> data =
				Arrays.asList(
						new TripDto(
								"1", "22-01-2018 13:00:00", "ON", "Stop1", "Company1", "Bus37", "5500005555555559"),
						new TripDto(
								"2",
								"22-01-2018 13:05:00",
								"OFF",
								"Stop1",
								"Company1",
								"Bus37",
								"5500005555555559"));

		List<Trip> actual = service.processTravelRecords(data);
		assertEquals(actual.size(), 1);
		assertNull(actual.get(0).getPrice());
	}

	@Test
	void shouldThrowEntityNotFoundExceptionForInvalidBusId() {
		List<TripDto> data =
				Arrays.asList(
						new TripDto(
								"1",
								"22-01-2018 13:00:00",
								"ON",
								"Stop1",
								"Company1",
								"junk",
								"5500005555555559"));
		assertThrows(EntityNotFoundException.class, () -> service.processTravelRecords(data));
	}

	@Test
	void shouldThrowEntityNotFoundExceptionForInvalidStopId() {
		List<TripDto> data =
				Collections.singletonList(
						new TripDto(
								"1", "22-01-2018 13:00:00", "ON", "junk", "Company1", "Bus37", "5500005555555559"));
		assertThrows(EntityNotFoundException.class, () -> service.processTravelRecords(data));
	}
}
