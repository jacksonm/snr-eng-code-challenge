package org.example.app.service;

import org.example.app.service.dto.TripDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CsvServiceTest {

	@Autowired private DateTimeFormatter formatter;

	private CsvService service;

	@BeforeAll
	public void setUp() {
		service = new CsvService(formatter);
	}

	@Test
	void shouldReadCsvFile() throws IOException {
		List<TripDto> actual = service.parseTravelData("src/test/resources/csv/input.csv");

		assertTripDto(actual.get(0), "1", "22-01-2018 01:00:00", "OFF", "Stop1");
		assertTripDto(actual.get(1), "2", "22-01-2018 02:00:00", "ON", "Stop1");
		assertTripDto(actual.get(2), "2", "22-01-2018 02:05:00", "OFF", "Stop2");
	}

	private void assertTripDto(TripDto t, String id, String dt, String tt, String sid) {
		assertEquals(t.id, id);
		assertEquals(t.dateTime, dt);
		assertEquals(t.tapType, tt);
		assertEquals(t.stopId, sid);
	}
}
