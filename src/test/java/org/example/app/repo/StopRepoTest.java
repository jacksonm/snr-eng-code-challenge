package org.example.app.repo;

import org.example.app.entity.Stop;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class StopRepoTest {

  @Autowired private StopRepo stopRepo;

  @Test
  void shouldGetStopById() {
    Optional<Stop> actual = stopRepo.getStopById("Stop1");
    assertTrue(actual.isPresent());
  }

  @Test
  void shouldGetInvalidStopById() {
    Optional<Stop> actual = stopRepo.getStopById("junk");
    assertFalse(actual.isPresent());
  }
}
