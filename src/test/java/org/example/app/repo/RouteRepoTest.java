package org.example.app.repo;

import org.example.app.entity.Route;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class RouteRepoTest {

  @Autowired private RouteRepo routeRepo;

  @Test
  void shouldGetRouteByStopIds() {
    List<String> stops = new ArrayList<>();
    stops.add("Stop1");
    stops.add("Stop2");

    Route actual = routeRepo.getRouteByStopIds(stops).get();
    assertEquals(actual.getPrice(), 3.25d);
  }

  @Test
  void shouldGetInvalidRouteByStopIds() {
    List<String> stops = new ArrayList<>();
    stops.add("Stop1");
    stops.add("junk");

    Optional<Route> actual = routeRepo.getRouteByStopIds(stops);
    assertFalse(actual.isPresent());
  }

  @Test
  void shouldGetRouteWithHighestPriceByStopId() {
    Route actual = routeRepo.getHighestPriceByStopId("Stop1").get();
    assertEquals(actual.getPrice(), 7.30d);
  }
}
