package org.example.app.repo;

import org.example.app.entity.Company;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class CompanyRepoTest {

  @Autowired private CompanyRepo companyRepo;

  @Test
  void shouldGetCompanyById() {
    Optional<Company> actual = companyRepo.getById("Company1");
    assertTrue(actual.isPresent());
  }

  @Test
  void shouldGetInvalidCompanyById() {
    Optional<Company> actual = companyRepo.getById("junk");
    assertFalse(actual.isPresent());
  }
}
