package org.example.app.repo;

import org.example.app.entity.Bus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class BusRepoTest {

  @Autowired private BusRepo busRepo;

  @Test
  void shouldGetBusById() {
    Optional<Bus> actual = busRepo.getById("Bus37");
    assertTrue(actual.isPresent());
  }

  @Test
  void shouldGetInvalidBusById() {
    Optional<Bus> actual = busRepo.getById("junk");
    assertFalse(actual.isPresent());
  }
}
