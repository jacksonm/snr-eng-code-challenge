package org.example.app.util;

public class Util {

  public String readFile(String filename) {
    return this.getClass().getResource(filename).toString();
  }
}
