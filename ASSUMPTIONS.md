# Assumptions

1. Input data is well always formatted
1. If first trip is a tap OFF, the trip is treated as if the user forgot to tap ON. `TripStatus.INCOMPLETE`
1. If last trip is a tap ON, the trip is treated as if the user forgot to tap OFF. `TripStatus.INCOMPLETE`
1. Cancelled trips are still complete. `TripStatus.COMPLETE`. A new enum could be introduced for this state; `CANCELLED`.
1. All `companies`, `buses`, `stops`, and `routes` are already existing in the data store

